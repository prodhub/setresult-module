#SetResult

Module to set result validation


## Install

```sh
$ npm install --save @adwatch/setresult
```


## Usage

```js
import SetResult from '@adwatch/setresult';
// or
var SetResult = require('@adwatch/setresult/build');

//Initialization module
let setResult = new SetResult(options);
```


## API


####setErrorsTo(array)

Set errors to inputs of form

```js
let result = [
	{
		elem:$('input[name="password1"]'),
		status:"empty",
		type:"required",
		val:"",
		validation:"error"
	},
	{
		elem:$('input[name="password2"]'),
		status:"invalid",
		type:"equal",
		val:"32154",
		validation:"error"
	}
];

setTimeout(function(){
	setResult.setErrorsTo(result);
}, 2000);
```


####clearErrors()

Clear Errors in your form

*It can be take disposable optional params*

```js
//Clear all Errors containers
setResult.clearErrors();

//Clear jquery elem
setResult.clearErrors($('input'));

//Clear array of selectors
setResult.clearErrors(['input[name="first_name"]', 'select']);
```


## OPTIONS

####$blockForm
Type `string`

**It is necessary option**

It defines your form selector and other required dependencies
```js
let setResult = new SetResult({
	$blockForm: '#myForm'
});
```


####clsErrorForm
Type `string`

Default: `'error'`

Set error class to your form
```js
setResult.clsErrorForm = 'myError';
```


####clsSuccessForm
Type `string`

Default: `'success'`

Set success class to your form
```js
setResult.clsSuccessForm = 'mySuccess';
```


####clsErrorInput
Type `string`

Default: `'error'`

Set error class to your inputs of form
```js
setResult.clsErrorInput = 'myError';
```


####clsSuccessInput
Type `string`

Default: `''`

Set success class to your inputs of form
```js
setResult.clsSuccessInput = 'mySuccess';
```


####setErrors
Type `object`

Default: `false`

Bind target parent node and target error node
```js
setResult.targetParent = '.form__group';
setResult.targetError = '.form__msg';
```


####ERRORS_MSG
Type `object`

Default: `false`

Set errors messages to specific types
```js
setResult.ERRORS_MSG.required.empty = 'Wasted...';
setResult.ERRORS_MSG.password.invalid = 'You shall not pass!!!';
```

**Table of values by default**

| data-validation | Description |
|:------:|:-----------:|
|required.empty|Значение не должно быть пустым|
|select.empty|Значение не должно быть пустым|
|email.empty|Значение не должно быть пустым|
|email.invalid|Неправильный формат|
|password.empty|Значение не должно быть пустым|
|password.invalid|Пароль Слишком простой|
|equal.empty|Значение не должно быть пустым|
|equal.invalid|Значения не совпадают|
|keyError.oC|Разрешены только кириллические символы|
|keyError.oL|Разрешены только латинские символы|
|keyError.oN|Разрешены только цифры|
|keyError.oE|Разрешены только цифры, спецсимволы и символы латинского алфавита|


 ## License

 MIT ©
